from django.conf.urls import patterns, include, url
from bell_canada_cleansematch_interface.apps.interface import views

urlpatterns = patterns('',
    url(r'^match/?$', views.match, name='match'),
)