from django.db import models


class SavedAuth(models.Model):
    api_user = models.CharField(max_length=64)
    api_pass_hash = models.CharField(max_length=64)
    token = models.CharField(max_length=512)
    refresh_time = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('api_user', 'api_pass_hash')
