from django.http.response import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from lxml import etree
import re
import requests
from models import SavedAuth as AuthCache
from django.utils.timezone import utc
import hashlib
import datetime
logEntry = ""

@csrf_exempt
def match(request):
    start_time = datetime.datetime.now()
    if request.method != 'POST':
        return HttpResponse(status=405)

    print 'RECEIVED POST - BODY: \n%s' % (request.body)

    etree_time = datetime.datetime.now()
    global logEntry
    logEntry = ""

    logEntry += "\nStart Time -------- " + str(datetime.datetime.now()) + "\n"
    try:
        root = etree.fromstring(request.body)
    except:
        return _produce_error_response('Post body does not contain valid XML', '400')

    etree_proc_time = datetime.datetime.now() - etree_time
    # _log_this("Start Time -------- ", datetime.datetime.now())
    # _log_this("etree_proc_time", etree_proc_time)
    logEntry += "etree_proc_time " + str(etree_proc_time) + "\n"
    control = root.xpath('Control')
    if len(control) != 1:
        return _produce_error_response("XML request must contain exactly 1 'Control' element", '400')

    control = control[0]

    # Pull control section data - all required
    match_request = {
        'api_uid': _get_element_text(control, 'UserID'),
        'api_pwd': _get_element_text(control, 'Password'),
        'transaction_id': _get_element_text(control, 'TransactionID')
    }

    if not match_request['api_uid'] or not match_request['api_pwd'] or not match_request['transaction_id']:
        return _produce_error_response("XML 'Control' element must contain 'UserID', 'Password', and 'TransactionID'", '400')

    # Pull remaining data
    match_request['business_name'] = _get_element_text(root, 'BusinessName')
    match_request['street_address'] = _get_element_text(root, 'StreetAddress')
    match_request['city'] = _get_element_text(root, 'City')
    match_request['territory_abbreviation'] = _get_element_text(root, 'TerritoryAbbreviation')
    match_request['postal_code'] = _get_element_text(root, 'PostalCode')
    match_request['country_code'] = _get_element_text(root, 'CountryCode')
    match_request['telephone_number'] = _get_element_text(root, 'TelephoneNumber')
    try:
        match_request['confidence_threshold'] = int(_get_element_text(root, 'ConfidenceThreshold'))
    except:
        match_request['confidence_threshold'] = 7

    auth_token_time = datetime.datetime.now()
    auth_token = None
    try:
        auth_token = _get_direct_auth(match_request['api_uid'], match_request['api_pwd'])
    except Exception as e:
        return _produce_error_response('Unauthorized', str(e))

    auth_token_proc_time = datetime.datetime.now() - auth_token_time

    # _log_this("auth_token_proc_time", auth_token_proc_time)
    logEntry += "auth_token_proc_time " + str(auth_token_proc_time) + "\n"
    try:
        _direct_match_time = datetime.datetime.now()
        result = _direct_match(auth_token, match_request)
        _direct_match_proc_time = datetime.datetime.now() - _direct_match_time
        # _log_this("_direct_match_proc_time", _direct_match_proc_time)
        logEntry += "_direct_match_proc_time " + str(_direct_match_proc_time) + "\n"
        _direct_custom_packet_time = datetime.datetime.now()
        result = _direct_custom_packet(auth_token, result)
        _direct_custom_packet_proc_time = datetime.datetime.now() - _direct_custom_packet_time
        # _log_this("_direct_custom_packet_proc_time", _direct_custom_packet_proc_time)
        logEntry += "_direct_custom_packet_proc_time " + str(_direct_custom_packet_proc_time) + "\n"
    except DirectException as error:
        return _produce_error_response(error.result_text, error.result_code, error.direct_code, error.severity_text, match_request['transaction_id'])

    produce_xml_response_time = datetime.datetime.now()
    _result_response = _produce_xml_response(result, match_request['transaction_id'])
    produce_xml_response_proc_time = datetime.datetime.now() - produce_xml_response_time
    # _log_this("produce_xml_response_proc_time", produce_xml_response_proc_time)
    logEntry += "produce_xml_response_proc_time " + str(produce_xml_response_proc_time) + "\n"
    total_time = datetime.datetime.now() - start_time
    # _log_this("-------------------------------------- Total time", total_time)
    logEntry += "-------------------------------------- Total time " + str(total_time) + "\n"
    _log_this(logEntry, 0)
    return _result_response


def _get_direct_auth(uid, pwd):
    pass_hash = hashlib.sha256(pwd).hexdigest()
    global logEntry
    # _log_this("Start Username : " + uid + " Password : " + pwd, 0)
    logEntry += "Start Username : " + uid + " Password : " + pwd + "\n"

    # Check for cached token that is still valid
    try:
        catched_token = AuthCache.objects.get(api_user=uid, api_pass_hash=pass_hash)
        now = datetime.datetime.now().replace(tzinfo=utc)
        dif = now - catched_token.refresh_time
        if dif.total_seconds() < 14400: # 4 hrs
            # _log_this("Cache HIT", dif)
            logEntry += "cached_token.refresh_time : " + str(catched_token.refresh_time) + "\n"
            logEntry += "now : " + str(now) + "\n"
            logEntry += "Cache HIT time_dif : " + str(dif.total_seconds()) + "\n"
            return catched_token.token
        else:
            catched_token.delete()
    except Exception as e:
        print(e)
        pass

    # _log_this("Cache Miss", 0)
    logEntry += "Cache Miss " + "\n"
    url = 'https://direct.dnb.com/rest/Authentication'
    headers = {
        'x-dnb-user': uid,
        'x-dnb-pwd': pwd,
    }

    response = requests.post(url, headers=headers)
    if response.status_code != 200:
        # _log_this("Authentication Error - End Username : " + uid + " Password : " + pwd, 0)
        logEntry += "Authentication Error - End Username : " + uid + " Password : " + pwd + "\n"
        raise Exception(response.status_code)

    token = response.headers['authorization']
    AuthCache(api_user=uid, api_pass_hash=pass_hash, token=token).save()

    return token

def _direct_match(auth_token, match_request):
    url = 'https://direct.dnb.com/V4.0/organizations?cleansematch'
    params = {
        'SubjectName': match_request['business_name'],
        'StreetAddressLine-1': match_request['street_address'],
        'PrimaryTownName': match_request['city'],
        'TerritoryName': match_request['territory_abbreviation'],
        'FullPostalCode': match_request['postal_code'],
        'CountryISOAlpha2Code': match_request['country_code'],
        'TelephoneNumber': match_request['telephone_number'],
        'CandidateMaximumQuantity': 1,
        'ConfidenceLowerLevelThresholdValue': match_request['confidence_threshold'],
    }

    headers = {
        'authorization': auth_token
    }

    response = requests.get(url, params=params, headers=headers)
    response_json = response.json()
    if response.status_code != 200:
        try:
            raise DirectException(response.status_code, response_json['GetCleanseMatchResponse']['TransactionResult'])
        except KeyError:
            raise DirectException('500')

    try:
        match = response_json['GetCleanseMatchResponse']['GetCleanseMatchResponseDetail']['MatchResponseDetail']['MatchCandidate'][0]
    except:
        raise DirectException('500')

    match_result = {
        'organization_name': _get_json_field(match, 'OrganizationPrimaryName|OrganizationName|$'),
        'duns_number': _get_json_field(match, 'DUNSNumber'),
        'confidence_score': _get_json_field(match, 'MatchQualityInformation|ConfidenceCodeValue'),
    }

    return match_result

def _direct_custom_packet(auth_token, match_result):
    url = 'https://direct.dnb.com/V2/organizations/%s/products/CST_PRD_3' % (match_result['duns_number'])

    headers = {
        'authorization': auth_token
    }

    response = requests.get(url, headers=headers)

    response_json = response.json()

    if response.status_code != 200:
        try:
            raise DirectException(response.status_code, response_json['OrderProductResponse']['TransactionResult'])
        except KeyError:
            raise DirectException('500')

    try:
        match = response_json['OrderProductResponse']['OrderProductResponseDetail']['Product']['Organization']
    except:
        raise DirectException('500')

    telephone_number = _get_json_field(match, 'Telecommunication|TelephoneNumber[0]|TelecommunicationNumber')
    if telephone_number:
        telephone_number = re.sub('\D', '', telephone_number)

    data = {
        'business_name': _get_json_field(match, 'OrganizationName|OrganizationPrimaryName[0]|OrganizationName|$'),
        'street_address': _get_json_field(match, 'Location|PrimaryAddress[0]|StreetAddressLine[0]|LineText'),
        'city': _get_json_field(match, 'Location|PrimaryAddress[0]|PrimaryTownName'),
        'territory_abbreviation': _get_json_field(match, 'Location|PrimaryAddress[0]|TerritoryAbbreviatedName'),
        'territory': _get_json_field(match, 'Location|PrimaryAddress[0]|TerritoryOfficialName'),
        'postal_code': _get_json_field(match, 'Location|PrimaryAddress[0]|PostalCode'),
        'country_code': _get_json_field(match, 'Location|PrimaryAddress[0]|CountryISOAlpha2Code'),
        'country_name': _get_json_field(match, 'Location|PrimaryAddress[0]|CountryOfficialName'),
        'telephone_number': telephone_number,
        'ccs_raw_score': _get_json_field(match, 'Assessment|CommercialCreditScore[0]|RawScore'),
        'fss_raw_score': _get_json_field(match, 'Assessment|FinancialStressScore[0]|RawScore'),
        'business_start_year': _get_json_field(match, 'OrganizationDetail|OrganizationStartYear'),
        'sic_code': None, #initialize
        'sic_description': None, #initialize
    }

    # If business name was not returned in the custom packet call, use the name that was received in the match.
    if not data['business_name']:
        data['business_name'] = match_result['organization_name']

    # Grab US SIC Code Only
    industries = _get_json_field(match, 'IndustryCode|IndustryCode')
    if industries:
        for industry in industries:
            code_value = _get_json_field(industry, '@DNBCodeValue')
            if code_value == 2831: # DNB Code for 'Canada Standard Industry Code 1977'
                display_sequence = _get_json_field(industry, 'DisplaySequence')
                if display_sequence and display_sequence == 1:
                    data['sic_code'] = _get_json_field(industry, 'IndustryCode|$')
                    if data['sic_code']:
                        data['sic_code'] = str(data['sic_code'])[:4]
                    data['sic_description'] = _get_json_field(industry, 'IndustryCodeDescription[0]|$')
                    break

    for key in data:
        match_result[key] = data[key]

    return match_result

def _produce_xml_response(data, transaction_id):
    """Returns an HttpResponse with body in the following form:
        <response>
            <Control>
                <StatusCode>CM000</StatusCode>
                <StatusSeverity>Information</StatusSeverity>
                <StatusMessage>Success</StatusMessage>
                <TransactionID>xxxx-xxxx</TransactionID>
            </Control>
            <DUNS>#########</DUNS>
            <BusinessName>Business Example</BusinessName>
            <StreetAddress>123 FakeStreet</StreetAddress>
            <City>ExCity</City>
            <TerritoryAbbreviation>ON</TerritoryAbbreviation>
            <Territory>Ontario</Territory>
            <PostalCode>M1P5E4</PostalCode>
            <CountryCode>CA</CountryCode>
            <CountryName>Canada</CountryName>
            <TelephoneAreaCode>416</TelephoneAreaCode>
            <TelephoneNumber>1231234</TelephoneNumber>
            <BusinessStartYear>2003</BusinessStartYear>
            <ConfidenceScore>8</ConfidenceScore>
            <CCSRawScore>100</CCSRawScore>
            <FSSRawScore>1000</FSSRawScore>
            <SICCode>2752</SICCode>
            <SICDescription>Lithographic commercial printing</SICDescription>
        </response>
    """
    response = etree.Element('response')

    # Add control information
    control = etree.SubElement(response, 'Control')
    status_code = etree.SubElement(control, 'StatusCode')
    status_severity = etree.SubElement(control, 'StatusSeverity')
    status_message = etree.SubElement(control, 'StatusMessage')
    x_transaction_id = etree.SubElement(control, 'TransactionID')

    status_code.text = 'CM000'
    status_severity.text = 'Information'
    status_message.text = 'Success'
    x_transaction_id.text = transaction_id

    # Convert scores to strings
    if data['ccs_raw_score'] is not None:
        data['ccs_raw_score'] = str(data['ccs_raw_score'])

    if data['fss_raw_score'] is not None:
        data['fss_raw_score'] = str(data['fss_raw_score'])

    # Add data
    duns = etree.SubElement(response, 'DUNS')
    business_name = etree.SubElement(response, 'BusinessName')
    street_address = etree.SubElement(response, 'StreetAddress')
    city = etree.SubElement(response, 'City')
    territory_abbreviation = etree.SubElement(response, 'TerritoryAbbreviation')
    territory = etree.SubElement(response, 'Territory')
    postal_code = etree.SubElement(response, 'PostalCode')
    country_code = etree.SubElement(response, 'CountryCode')
    country_name = etree.SubElement(response, 'CountryName')
    telephone_area_code = etree.SubElement(response, 'TelephoneAreaCode')
    telephone_number = etree.SubElement(response, 'TelephoneNumber')
    business_start_year = etree.SubElement(response, 'BusinessStartYear')
    confidence_score = etree.SubElement(response, 'ConfidenceScore')
    ccs_raw_score = etree.SubElement(response, 'CCSRawScore')
    fss_raw_score = etree.SubElement(response, 'FSSRawScore')
    sic_code = etree.SubElement(response, 'SICCode')
    sic_description = etree.SubElement(response, 'SICDescription')

    duns.text = data['duns_number']
    business_name.text = data['business_name']
    street_address.text = data['street_address']
    city.text = data['city']
    territory_abbreviation.text = data['territory_abbreviation']
    territory.text = data['territory']
    postal_code.text = data['postal_code']
    country_code.text = data['country_code']
    country_name.text = data['country_name']
    business_start_year.text = data['business_start_year']
    confidence_score.text = str(data['confidence_score'])
    ccs_raw_score.text = data['ccs_raw_score']
    fss_raw_score.text = data['fss_raw_score']
    sic_code.text = data['sic_code']
    sic_description.text = data['sic_description']

    if data['telephone_number'] and len(data['telephone_number']) > 3:
        telephone_area_code.text = data['telephone_number'][:3]
        telephone_number.text = data['telephone_number'][3:]

    # Remove empty tags
    for child in response:
        if child.tag == 'Control':
            continue
        if child.text == None or len(child.text) == 0:
            response.remove(child)

    return HttpResponse(etree.tostring(response, pretty_print=True), status=200)

def _produce_error_response(message, http_code, error_code=None, severity='Error', transaction_id=None):
    """Returns an HttpResponse with body in the following form:
        <response>
            <Control>
                <StatusCode>CM008</StatusCode>
                <StatusSeverity>Error</StatusSeverity>
                <StatusMessage>No match found for the requested Duns number.</StatusMessage>
                <TransactionID>xxxx-xxxx</TransactionID>
            </Control>
        </response>
    """
    if not error_code:
        error_code = http_code

    try:
        http_code = int(http_code)
    except:
        http_code = 500

    response = etree.Element('response')
    control = etree.SubElement(response, 'Control')
    status_code = etree.SubElement(control, 'StatusCode')
    status_severity = etree.SubElement(control, 'StatusSeverity')
    status_message = etree.SubElement(control, 'StatusMessage')
    x_transaction_id = etree.SubElement(control, 'TransactionID')

    status_code.text = error_code
    status_severity.text = severity
    status_message.text = message
    x_transaction_id.text = transaction_id

    global logEntry
    logEntry += etree.tostring(response, pretty_print=True) + "\n **************************** \n"
    _log_this(logEntry, 0)
    return HttpResponse(etree.tostring(response, pretty_print=True), status=http_code)

# Utility code below here
# ------------------------------------------------

class DirectException(Exception):
    def __init__(self, response_code, transaction_result=None):
        self.result_code = response_code
        if not transaction_result:
            self._initDefault()
            return

        try:
            self.severity_text = transaction_result['SeverityText']
            self.direct_code = transaction_result['ResultID']
            self.result_text = transaction_result['ResultText']
        except:
            self._initDefault()

    def _initDefault(self):
        self.severity_text = 'Error'
        self.direct_code = 'Unknown'
        self.result_text = 'An unknown error occurred.'

# Can separate keys by pipes, for instance result['OrganizationPrimaryName']['OrganizationName'] is equivalent to _get_json_field('OrganizationPrimaryName|OrganizationName')
# Can also handle arrays.  result['array'][1]['deeper'] is equivalent to _get_json_field('array[1]|deeper')
def _get_json_field(json, key, default=None):
    if '|' not in key:
        try:
            if '[' in key:
                index = int(key[key.index('[')+1 : key.index(']')])
                key = key[:key.index('[')]

                return json[key][index]
            else:
                return json[key]
        except:
            return default
    else:
        try:
            nextKey = key[key.index('|')+1:]
            key = key[:key.index('|')]
            if '[' in key:
                index = int(key[key.index('[')+1 : key.index(']')])
                key = key[:key.index('[')]

                return _get_json_field(json[key][index], nextKey)
            else:
                return _get_json_field(json[key], nextKey)
        except:
            return default

# Get the XML element text, else return None.
def _get_element_text(element, xpath):
    try:
        return element.xpath(xpath)[0].text
    except:
        return None


# Write string to log file
def _log_this(proc_name,time_log):
    try:
        fo = open("api_logs.txt", "a")
        if time_log != 0:
            fo.write(str(proc_name) + " : " + str(time_log) + "\n")
        else:
            fo.write(str(proc_name) + "\n")

        fo.close()
        return True
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)
    except ValueError:
        print "Could not convert data to an integer."

    return False
